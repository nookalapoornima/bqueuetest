CC = gcc -g -DARM #-DPOWER9 #-DARM
CFLAGS = -O3 -Wall -Wpedantic -Werror 
#CFLAGS += -DCONS_BATCH
#CFLAGS += -DBACKTRACKING
#CFLAGS += -DADAPTIVE
#CFLAGS = -O2 -pthread -std=c99
OBJECTS = fifo.o main.o

#hack: main.c
#	$(CC) -O2 -pthread fifo.c main.c -lpthread -lrt -o main

all: main

main: $(OBJECTS)
	$(CC) $(OBJECTS) -lrt -lpthread -o main

main.o: main.c
	$(CC) $(CFLAGS) -c main.c

fifo.o: fifo.c
	$(CC) $(CFLAGS) -c fifo.c

clean:
	rm -f *.o main
