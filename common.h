
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sched.h>
#include <omp.h>
#include <time.h>
#include <inttypes.h>
#include <string.h>

typedef unsigned long long ticks;

#ifdef POWER9

static unsigned cyc_hi = 0;
static unsigned cyc_lo = 0;
static long tbcycles = 0;

static long myround(double u)
{
  long result = u;
  while (result + 0.5 < u) result += 1;
  while (result - 0.5 > u) result -= 1;
  return result;
}

static long long microseconds(void)
{
  struct timeval t;
  gettimeofday(&t,(struct timezone *) 0);
  return t.tv_sec * (long long) 1000000 + t.tv_usec;
}

static __inline__ ticks getticks()
{
	/*
	 * Use the performance monitoring functionality of any PowerPC 604 or later
	 * to count cycles. 
	 */

	register unsigned temp; // just to be sure, for the assembly's sake

#if PPC_SUPERVISOR
	/* Version 1- uses supervisor-level MMCR and PMC for exact cycle count */

	// if I'm understanding PPC ASM right, this should stop any timing currently
	// running (MMCR=0) ans clear PMC1.
	asm("li %0,0\nmtspr MMCR0,%0\nmtspr PMC1,%0":"=r"(temp));

	//initialize performance monitoring system, tell PMC1 to count cycles by setting
	//proper bits of MMCR0, grab initial value in cyc_lo:
	// Load Immediate (16bit) 64 to temp, Move temp To Special Purpose Reg MMCR0, Move temp From SPR MMCR0
	asm("li %0,64\nmtspr MMCR0,%0\nmfspr %0,PMC1":"=r"(temp));
#else
	/* Version 2- uses user-accessible TimeBase Lower register- TB increments once
	 * every n cycles, n is processor dependent: 
	 * PPC 604 n~4 
	 * Wallstreet G3 266 n=16 
	 * Titanium 15" Ghz G4 n=30
	 * 1.8GHz G5 n~50
	 */

	asm("mftb %0":"=r"(temp));
#endif

	cyc_lo = temp;
	cyc_hi = cyc_lo; 
	return cyc_lo;
}

static __inline__ ticks getticks_cpuid()
{
	register unsigned temp;
#if PPC_SUPERVISOR
	// copy PMC1 counter value to cyc_hi.  Not ideal- mfspr takes time.
	asm("mfspr %0,PMC1":"=r"(temp));
	cyc_hi=temp;
	if (cyc_hi<cyc_lo){
		fprintf(stderr, "Error: Final cycle count > initial cycle count.\n");
	}
#else
	asm("mftb %0":"=r"(temp));
	cyc_hi=temp;
#endif
	return (cyc_hi);
}

static __inline__ int calibrate_time_base(void) {
	 static double cpufrequency = 0;

        FILE *f;
        int s;
        long long tb0; long long us0;
        long long tb1; long long us1;

        f = fopen("/proc/cpuinfo","r");
        if (!f) return 0;

        for (;;) {
                s = fscanf(f," clock : %lf MHz",&cpufrequency);
                if (s > 0) break;
                if (s == 0) s = fscanf(f,"%*[^\n]\n");
                if (s < 0) { cpufrequency = 0; break; }
        }
        fclose(f);
        if (!cpufrequency) return -1;
        cpufrequency *= 1000000.0;

        tb0 = getticks();
        us0 = microseconds();
        do {
                tb1 = getticks();
                us1 = microseconds();
        } while (us1 - us0 < 10000);
        if (tb1 <= tb0) return -1;

        tb1 -= tb0;
        us1 -= us0;
        tbcycles = myround((cpufrequency * 0.000001 * (double) us1) / (double) tb1);
	printf("time base cycles %ld\n", tbcycles);
	return 0;
}

#else /* Power9 End */

#ifndef ARM
#ifdef PHI

//get number of ticks, could be problematic on modern CPUs with out of order execution

static __inline__ ticks getticks(void) {
	ticks tsc;
	__asm__ __volatile__(
			"rdtsc;"
			"shl $32, %%rdx;"
			"or %%rdx, %%rax"
			: "=a"(tsc)
			:
			: "%rcx", "%rdx");

	return tsc;
}
#else

static __inline__ ticks getticks(void) {
	ticks tsc;
	__asm__ __volatile__(
			"rdtscp;"
			"shl $32, %%rdx;"
			"or %%rdx, %%rax"
			: "=a"(tsc)
			:
			: "%rcx", "%rdx");

	return tsc;
}

#endif
static __inline__ ticks getticks_cpuid(void) {
	register unsigned a, d;
	__asm__ __volatile__ (
			"cpuid\n\t"
			"rdtsc\n\t"
			: "=a" (a),
			"=d" (d)
			:
			: "ebx", "ecx"
			);
	return (((uint64_t) d << 32) | a);
}

#else

/*static void enable_ccnt_read(void* data) {
	// WRITE PMUSERENR = 1
	//__asm__ __volatile__ ("mcr p15, 0, %0, c9, c14, 0\n\t" : : "r" (1));

	uint32_t pmccntr;
	uint32_t pmuseren;
	uint32_t pmcntenset;
	// Read the user mode perf monitor counter access permissions.
	__asm__ __volatile__ ("mrc p15, 0, %0, c9, c14, 0" : "=r" (pmuseren));
	printf("pmuseren : %d\n", pmuseren);
	if (pmuseren & 1) { // Allows reading perfmon counters for user mode code.
		{
			__asm__ __volatile__ ("mrc p15, 0, %0, c9, c12, 1" : "=r" (pmcntenset));
			printf("pmcntenset : %d\n", pmcntenset);
		}
		if (pmcntenset & 0x80000000ul) { // Is it counting?
			__asm__ __volatile__ ("mrc p15, 0, %0, c9, c13, 0" : "=r" (pmccntr));
			// The counter is set up to count every 64th cycle
			return pmccntr << 6;
		}
	}
}*/

static uint64_t cntrFreq;

static __inline__ void getfreq() {
        __asm__ __volatile__ ("mrs %0, cntfrq_el0" : "=r" (cntrFreq));
	printf("PMU Counter Frequency is %ld\n", cntrFreq);
}

static __inline__ ticks getticks() {
    volatile ticks cc = 0;
    __asm__ __volatile__ ("mrs %0, cntvct_el0" : "=r" (cc));
    return cc;
}

static __inline__ ticks getticks_cpuid() {
    volatile ticks cc = 0;
    __asm__ __volatile__ ("mrs %0, cntvct_el0" : "=r" (cc));
    return cc;
}

#endif
#endif

int cmpfunc(const void * a, const void * b) {
	return ( *(ticks*) a - *(ticks*) b);
}

void SortTicks(ticks* numTicks, int total) {
	qsort(numTicks, total, sizeof (*numTicks), cmpfunc);
}

void ComputeThroughputSummary(char* type, int numThreads, double throughput, int numSamples, float clockFreq, char* rawfilename, char* aggfilename)
{
	FILE *afp = fopen(aggfilename, "a");
	printf("%s %d %f %d %f\n", type, numThreads, clockFreq, numSamples, throughput);
	fprintf(afp, "%s %d %f %d %f\n", type, numThreads, clockFreq, numSamples, throughput);

}

void ComputeLatencySummary(char* type, int numThreads, ticks* numTicks, int numSamples, float clockFreq, char* rawfilename, char* aggfilename) {
#ifdef LATENCY				    
	FILE *afp = fopen(aggfilename, "a");
	printf("Printing summary\n");
	ticks totalTicks = (ticks)0;
#ifdef ARM
       	for(int i=0; i< numSamples; i++) {
                 numTicks[i] = round((numTicks[i] * clockFreq * 1000000000)/cntrFreq);
       	}
#elif POWER9
	for(int i=0; i<numSamples; i++) {
		numTicks[i] = numTicks[i] * tbcycles;
	}
#endif
	//compute the elapsed time per invocation, and find min and max
	SortTicks(numTicks, numSamples);

	for (int i = 0; i < numSamples; i++) {
		totalTicks += numTicks[i];
	}
	printf("total ticks %llu\n", totalTicks);
#ifdef RAW
	FILE *rfp = fopen(rawfilename, "a");
	for(int i = 0; i <= 100; i++)
	{
		int index = 0;
		if(i == 100) index = numSamples-1;
		else
		index = (i / 100.0) * numSamples;
		double elapsedTime = (numTicks[index] * 1.0 / clockFreq);

		fprintf(rfp, "%s %f %d %f %llu %d %lf\n", type, (i/100.0), numSamples, clockFreq, numTicks[index], numThreads, elapsedTime);
#ifdef VERBOSE
		printf("%s %f %d %f %llu %d %lf\n", type, (i/100.0), numSamples, clockFreq, numTicks[index], numThreads, elapsedTime);
#endif
	}
#endif

	ticks tickMin = numTicks[0];
	ticks tickMax = numTicks[numSamples - 1];

	//compute average
	double tickAverage = ((totalTicks * 1.0) / numSamples);
	ticks tickmedian = 0;

	if (numSamples % 2 == 0) {
		// if there is an even number of elements, return mean of the two elements in the middle
		tickmedian = ((numTicks[(numSamples / 2)] + numTicks[(numSamples / 2) - 1]) / 2.0);
	} else {
		// else return the element in the middle
		tickmedian = numTicks[(numSamples / 2)];
	}

	double minTime = ((tickMin * 1.0) / clockFreq);
	double maxTime = ((tickMax * 1.0) / clockFreq);
	double avgTime = ((tickAverage * 1.0) / clockFreq);


	printf("%s %d %f %d %llu %llu %llu %lf %lf %lf %lf\n", type, numThreads, clockFreq, numSamples, tickMin, tickMax, tickmedian, tickAverage, minTime, maxTime, avgTime);
	fprintf(afp, "%s %d %f %d %llu %llu %llu %lf %lf %lf %lf\n", type, numThreads, clockFreq, numSamples, tickMin, tickMax, tickmedian, tickAverage, minTime, maxTime, avgTime);
#endif
}

unsigned int rand_interval(unsigned int min, unsigned int max) {
	int r;
	const unsigned int range = 1 + max - min;
	const unsigned int buckets = RAND_MAX / range;
	const unsigned int limit = buckets * range;

	do {
		r = rand();
	} while (r >= limit);

	return min + (r / buckets);
}
