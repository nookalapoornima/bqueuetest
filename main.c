//---------------------------------------------------------------
// File: QueueMain.c
// Purpose: Main file with tests for a demonstration of a queue
//		as an array.
// Programming Language: C
// Author: Dr. Rick Coleman
// Date: February 11, 2002
//---------------------------------------------------------------
#define _GNU_SOURCE

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sched.h>
#include "fifo.h"
#include "common.h" 

typedef unsigned long long ticks;
#define NUM_THREADS 1
#define NUM_SAMPLES (1024 * 1024 * 1024)
#define NUM_CPUS 2

int* indata;
int* outdata;
//static int numEnqueue = 0;
//static int numDequeue = 0;
static ticks dequeue_ticks = 0, enqueue_ticks = 0;
static int CUR_NUM_THREADS = NUM_THREADS;
struct queue_t *q;
pthread_barrier_t barrier;

struct init_info {
	uint32_t	cpu_id;
	pthread_barrier_t * barrier;
};

struct init_info info[NUM_CPUS];

void *worker_handler( void * in)
{
	//INIT_INFO * init = (INIT_INFO *) arg;
	//int cpu_id = init->cpu_id;
	//pthread_barrier_t *barrier = init->barrier;

	ticks start_tick,end_tick,diff_tick;

	int NUM_SAMPLES_PER_THREAD = NUM_SAMPLES/CUR_NUM_THREADS;
	ELEMENT_TYPE value;
	pthread_barrier_wait(&barrier);

	start_tick = getticks();
	for (int i=1;i<=NUM_SAMPLES_PER_THREAD;i++)
	{
		while(dequeue(q, &value) != 0);
		if(value > NUM_SAMPLES_PER_THREAD || value < 0)
			printf("Verification error %lu\n", value);
		else
			outdata[value-1] = value;
		//printf("deq %d\n", (int)value);

	}
	end_tick = getticks();
	diff_tick = end_tick - start_tick;
	__sync_add_and_fetch(&dequeue_ticks,diff_tick);

	return 0;

}

void *enqueue_handler( void * in)
{
	//INIT_INFO * init = (INIT_INFO *) arg;
	//pthread_barrier_t *barrier = init->barrier;

	ticks start_tick,end_tick,diff_tick;

	int NUM_SAMPLES_PER_THREAD = NUM_SAMPLES/CUR_NUM_THREADS;
	pthread_barrier_wait(&barrier);

	start_tick = getticks();
	for (int i=1;i<=NUM_SAMPLES_PER_THREAD;i++)
	{	
		//printf("enq %d\n", i);
		while(enqueue(q, (ELEMENT_TYPE)i) != 0);
		indata[i-1] = i;
	}
	end_tick = getticks();
	diff_tick = end_tick - start_tick;
	__sync_add_and_fetch(&enqueue_ticks,diff_tick);

	return 0;

}


int main(int argc, char **argv)
{ 
	q = (struct queue_t*)malloc(sizeof(struct queue_t));
	queue_init(q);
	indata = malloc(sizeof(int) * NUM_SAMPLES);
	outdata = malloc(sizeof(int) * NUM_SAMPLES);
	//data = malloc(sizeof(int * NUM_SAMPLES));
	for(int i=0; i< NUM_SAMPLES; i++) {
		outdata[i] = 0;
		indata[i] = 0;
	}
#ifdef POWER9
	calibrate_time_base();
#endif
#ifdef ARM
	getfreq();
#endif
	for (int k=1;k<=NUM_THREADS;k=k*2)
		{
		
			CUR_NUM_THREADS = k;

			pthread_t *worker_threads;
			pthread_t *enqueue_threads;

			worker_threads = (pthread_t *) malloc(sizeof(pthread_t) * CUR_NUM_THREADS);
			enqueue_threads = (pthread_t *) malloc(sizeof(pthread_t) * CUR_NUM_THREADS);

			pthread_barrier_init(&barrier, NULL, CUR_NUM_THREADS * 2);

			for (int i = 0; i < CUR_NUM_THREADS; i++) {
				cpu_set_t set;
				CPU_ZERO(&set);
				CPU_SET(0, &set);
									
				pthread_attr_t attr;
												pthread_attr_init(&attr);
				pthread_create(&enqueue_threads[i], &attr, enqueue_handler, NULL);
				//cpu_set_t set;
				CPU_ZERO(&set);
				CPU_SET(48, &set);
									
				pthread_attr_t dattr;													pthread_attr_init(&dattr);				
				pthread_create(&worker_threads[i], &dattr, worker_handler, NULL);
			}

			for (int i = 0; i < CUR_NUM_THREADS; i++) {
				pthread_join(enqueue_threads[i], NULL);
				pthread_join(worker_threads[i], NULL);
			}

			printf("Verifying results...");

			for(int i=0; i<NUM_SAMPLES; i++)
			{
				if(indata[i] != outdata[i])
					printf("Invalid data at i = %d, in = %d, out = %d!\n", i, indata[i], outdata[i]);
			}
			printf("Done\n");
			printf("NumSamples, NumThreads, EnqCycleCount, DeqCycleCount\n");
			printf("%d,%llu,%llu,%d\n", NUM_SAMPLES, enqueue_ticks/NUM_SAMPLES,  dequeue_ticks/NUM_SAMPLES, CUR_NUM_THREADS);
		}
	return 0;
}
